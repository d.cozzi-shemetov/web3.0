<?php

header('Content-Type: text/html; charset=UTF-8');

$abilities = ['god' => 'бессмертие', 'fly' => 'левитация', 'idclip' => 'магия', 'fireball' => 'огонь'];

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
 
  $messages = array();

  if (!empty($_COOKIE['save'])) {
 
    setcookie('save', '', 100000);
   
    $messages[] = 'Спасибо, ваши данные сохранены!';
  }

 
  $errors = array();
  $errors['name'] = !empty($_COOKIE['name_error']);
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['date'] = !empty($_COOKIE['date_error']);
  $errors['sex'] = !empty($_COOKIE['sex_error']);
  $errors['limb'] = !empty($_COOKIE['limb_error']);
  $errors['biography'] = !empty($_COOKIE['biography_error']);
  $errors['checkonon'] = !empty($_COOKIE['checkon_error']);

  if ($errors['name']) {
 
    setcookie('name_error', '', 100000);

    if ($_COOKIE['name_error'] == "1") {
      $messages[] = '<div id="error">Пожалуйста, заполните поле Name.</div>';
    }
    else {
        $messages[] = '<div id="error">Только русские буквы без пробелов в поле Name.</div>';
    }
    
  }
  if ($errors['email']) {
     
      setcookie('email_error', '', 100000);
   
      if ($_COOKIE['email_error'] == "1") {
          $messages[] = '<div id="error">Пожалуйста, заполните поле Email.</div>';
      }
      else {
          $messages[] = '<div id="error">Воспользуйтесь шаблоном email: name@mail.domain</div>';
      }
  }
  if ($errors['date']) {
 
      setcookie('date_error', '', 100000);
 
      if ($_COOKIE['date_error'] == "1") {
          $messages[] = '<div id="error">Пожалуйста, выберете год рождения</div>';
      }
  }
  if ($errors['sex']) {
    
      setcookie('sex_error', '', 100000);
  
      if ($_COOKIE['sex_error'] == "1") {
          $messages[] = '<div id="error">Пожалйста, укажите Ваш пол.</div>';
      }
  }
  if ($errors['limb']) {
   
      setcookie('limb_error', '', 100000);
  
      if ($_COOKIE['limb_error'] == "1") {
          $messages[] = '<div id="error">Выберете количество конечностей.</div>';
      }
  }
  
  if ($errors['biography']) {
     
      setcookie('biography_error', '', 100000);
    
      if ($_COOKIE['biography_error'] == "1") {
          $messages[] = '<div id="error">Пожалуйста, напишите биографию.</div>';
      }
      else {
          $messages[] = '<div id="error">Слишком большой текст в Биографии, максимум - 158 символов.</div>';
      }
  }
  
  $values = array();
  $values['name'] = empty($_COOKIE['name_value']) ? '' : $_COOKIE['name_value'];
  $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
  $values['date'] = empty($_COOKIE['date_value']) ? '' : $_COOKIE['date_value'];
  $values['sex'] = empty($_COOKIE['sex_value']) ? '' : $_COOKIE['sex_value'];
  $values['limb'] = empty($_COOKIE['limb_value']) ? '' : $_COOKIE['limb_value'];
  $values['biography'] = empty($_COOKIE['biography_value']) ? '' : $_COOKIE['biography_value'];
  $values['checkon'] = empty($_COOKIE['checkon_value']) ? '' : $_COOKIE['checkon_value'];
 
  include('form.php');
}

else {

  $errors = FALSE;
  if (empty($_POST['name'])) {
  
      setcookie('name_error', '1', 0);
      setcookie('name_value', '', 0);
    $errors = TRUE;
  }
  else {
      if (!preg_match('/^[а-яА-Я]+$/u', $_POST['name'])) {
          setcookie('name_error', '2', 0);
          $errors = TRUE;
      }
   
    setcookie('name_value', $_POST['name'], time() + 365 * 24 * 60 * 60);
  }
  
  
  if (empty($_POST['email'])) {
    
      setcookie('email_error', '1', 0);
      setcookie('email_value', '', 0);
      $errors = TRUE;
  }
  else {
      if (!preg_match('/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/u', $_POST['email'])) {
          setcookie('email_error', '2', 0);
          $errors = TRUE;
      }
    
      setcookie('email_value', $_POST['email'], time() + 365 * 24 * 60 * 60);
  }
  
  if (empty($_POST['date'])) {
      setcookie('date_error', '1', 0);
      $errors = TRUE;
  }
  else {
      setcookie('date_value', (int)$_POST['date'], time() + 365 * 24 * 60 * 60);
  }
  
  
  if (empty($_POST['sex'])) {
      setcookie('sex_error', '1', 0);
      $errors = TRUE;
  }
  else {
      setcookie('sex_value', $_POST['sex'], time() + 365 * 24 * 60 * 60);
  }
  
  
  if (empty($_POST['limb'])) {
      setcookie('limb_error', '1', 0);
      setcookie('limb_value', '', 0);
      $errors = TRUE;
  }
  else {
      setcookie('limb_value', $_POST['limb'], time() + 365 * 24 * 60 * 60);
  }
  
  
  if (empty($_POST['biography'])) {
      setcookie('biography_error', '1', 0);
      setcookie('biography_value', '', 0);
      $errors = TRUE;
  }
  else {
      if (strlen($_POST['biography'])>158){
          setcookie('biography_error', '2', 0);
          $errors = TRUE;
      }
      setcookie('biography_value', $_POST['biography'], time() + 365 * 24 * 60 * 60);
  }
  if (empty($_POST['checkon'])) {
      setcookie('checkon_error', '1', 0);
      setcookie('checkon_value', '', 0);
      $errors = TRUE;
  }
  else {
      setcookie('chek_value', $_POST['checkon'], time() + 365 * 24 * 60 * 60);
  }


  if ($errors) {
   
    header('Location: index.php');
    exit();
  }
  else {
 
    setcookie('name_error', '', 100000);
    setcookie('email_error', '', 100000);
    setcookie('date_error', '', 100000);
    setcookie('sex_error', '', 100000);
    setcookie('limb_error', '', 100000);
    setcookie('biography_error', '', 100000);
    setcookie('checkon_error', '', 100000);
 
  }


  
  $user = 'u20309';
  $pass = '4340817';
  $db = new PDO('mysql:host=localhost;dbname=u20309', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
  
   $stmt = $db->prepare("INSERT INTO application (name, mail, sex, limb, biography) VALUES (:name, :mail, :date, :sex, :limb, :biography)");
   $stmt->bindParam(':name', $firstname);
   $stmt->bindParam(':mail', $nemail);
   $stmt->bindParam(':date', $date);
   $stmt->bindParam(':sex', $valsex);
   $stmt->bindParam(':limb', $numlimbs);
   $stmt->bindParam(':biography', $biography);

   $firstname = $_POST['name'];
   $nemail = $_POST['email'];
   $date = (int)$_POST['date'];
   $valsex = $_POST['sex'];
   $numlimbs = (int)$_POST['limb'];
   $biographyg = $_POST['biography'];
   $stmt->execute();
   
  

  
  setcookie('save', '1');

  
  header('Location: index.php');
}

