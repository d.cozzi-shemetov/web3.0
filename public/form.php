<!DOCTYPE html>

<html lang="en">
<head>
    <title>web3.0</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <header>Форма</header>
    <form method="post" action="index.php">
        <!--1-->
        <label> Имя: <input type="text" name="name" placeholder="Введите имя"></label>

        <!--2-->
        <label> Email: <input type="email" name="email" placeholder="Введите email"></label>

        <!--3-->
        <label> Дата рождения: <input type="date" name="date"></label>

        <!--4-->
        <label>
            Пол: 
            <input type="radio" name="sex" value="Мужской"> Мужской
            <input type="radio" name="sex" value="Женский"> Женский
        </label>

        <!--5-->
        <label>
            Кол-во конечностей: 
            <input type="radio" name="limb" value="Значение 1"> 1
            <input type="radio" name="limb" value="Значение 2"> 2
            <input type="radio" name="limb" value="Значение 3"> 3
            <input type="radio" name="limb" value="Значение 4"> 4
        </label>

        <!--6-->
        <label>
            Сверхспособности: 
            <select name="super" multiple="multiple">
                <option value="Значение 1"> Невидимость </option>
                <option value="Значение 2"> Смотреть сквозь стены </option>
                <option value="Значение 3"> Контроль разума </option>
                <option value="Значение 4"> Способности метаморфа </option>
                <option value="Значение 5"> Изменение рельности </option>
            </select>
        </label>

        <!--7-->
        <label>
            Биография: 
            <textarea name="biography" placeholder="Введите информацию о себе"></textarea>
        </label>

        <!--8-->
        <label> С контрактом ознакомлен <input type="checkbox" name="checkon"> </label>

        <!--9-->
        <label><button type="submit"> Отправить </button></label>
    </form>
</body>
<!--The End-->
</html> 
